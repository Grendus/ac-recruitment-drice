// YOUR CODE HERE
const requestURL = "https://randomuser.me/api?results=10&nat=us";
const defaultMinAge = 18;
const defaultMaxAge = 90;

function LonelyHeartsDating() {
    let users = [];

    //Reads and returns the filter values as an object.
    this.parseForm = function() {
        let returnValue = {};
        returnValue.min = document.getElementById("ageMin").value;
        returnValue.max = document.getElementById("ageMax").value;
        returnValue.gender = document.querySelector('input[name="gender"]:checked').value;
        return returnValue;
    };

    //Resets the form to search for 18-90 year olds of either gender.
    this.resetForm = function() {
        document.getElementById("ageMin").value = defaultMinAge;
        document.getElementById("ageMax").value = defaultMaxAge;
        document.getElementById("radioButtonAny").checked = true;
    };

    //Requests users from the service
    this.getUsers = function() {
        let request = new XMLHttpRequest();
        let _this_ = this;
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200)
            {
                users = JSON.parse(request.responseText).results;
                _this_.showUsers();
            }
        };
        request.open("GET", requestURL, true);
        request.send(null);
    };

    //Parses the user's age based on their DOB compared to the current date.
    this.parseAge = function(dateString) {
        //A hacky solution, as it can be off by an hour due to some daylight savings times shenanigans.
        //For a production app, I'd use a library. For a demo, it's fine.
        let parsedDate = Date.parse(dateString);
        let ageDifMs = Date.now() - new Date(parsedDate).getTime();
        let ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    };

    //Returns true if the user matches our filter profile, false otherwise.
    this.filterUser = function(user, filter) {
        if (filter.gender === "any" || filter.gender === user.gender) {
            let age = this.parseAge(user.dob);
            return age >= filter.min && age <= filter.max;
        }
        return false;
    };

    //Returns HTML for rendering a user profile on the page.
    this.generateCandidate = function(user) {

        return `<li>
                <img class="photo" src="${user.picture.large}">
                <span class="name lead">${user.name.first} ${user.name.last}</span>
                <span class="age lead">${this.parseAge(user.dob)}</span>
                <button onclick="alert('Phone: ${user.phone}\\nCell: ${user.cell}\\nEmail: ${user.email}')">Contact</button>
              </li>`;
    };

    this.showUsers = function() {
        let filter = this.parseForm();
        let filteredUsers = users.filter(user=>LHD.filterUser(user, filter));
        let searchResultList = document.getElementById("searchResultList");
        searchResultList.innerHtml = '';
        let userList = '';
        filteredUsers.forEach(user=>userList+=LHD.generateCandidate(user));
        searchResultList.innerHTML = userList;

        if(filteredUsers.length===1)
            document.getElementById("searchResultsHeader").innerText = filteredUsers.length+" candidate found.";
        else
            document.getElementById("searchResultsHeader").innerText = filteredUsers.length+" candidated found.";

    }


}

const LHD = new LonelyHeartsDating();
try{
    //TODO: figure out why the HTML can't see the Javascript, but the Javascript can see the HTML. Until then, this is a hacky solution to connect them.
    document.getElementById("filter").addEventListener("click", function(){LHD.showUsers()});
    document.getElementById("reset").addEventListener("click", function(){LHD.resetForm(); LHD.showUsers()});
    LHD.getUsers();
}
catch(err){
    console.log(err.message)
}
//For unit testing.
module.exports = LonelyHeartsDating;