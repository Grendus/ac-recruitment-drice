// This unit test spec uses Jest: https://facebook.github.io/jest/docs/en/getting-started.html

//Initialize some DOM objects so index.html can load properly;
document.body.innerHTML =
    "          <button type=\"button\" class=\"btn btn-default\" id=\"reset\">Reset</button>\n" +
    "          <button type=\"submit\" class=\"btn btn-primary\" id=\"filter\">Filter</button>\n";

const LonelyHeartsDating = require('./index.js');

let LHD;

beforeEach(()=>{
    LHD = new LonelyHeartsDating();
});

test('Tests work', () => {
    expect(1 + 2).toBe(3);
});

test('Filters correctly on gender', ()=>{
    //Note: these tests will fail if your computer clock is set way off, or in 2073-ish.
    const dob = "1983-07-14 07:29:45";
    expect(LHD.filterUser({gender:'male', dob:dob}, {gender:'female', min:18, max:90})).toBe(false);
    expect(LHD.filterUser({gender:'male', dob:dob}, {gender:'male', min:18, max:90})).toBe(true);
    expect(LHD.filterUser({gender:'female', dob:dob}, {gender:'female', min:18, max:90})).toBe(true);
    expect(LHD.filterUser({gender:'female', dob:dob}, {gender:'male', min:18, max:90})).toBe(false);
    expect(LHD.filterUser({gender:'male', dob:dob}, {gender:'any', min:18, max:90})).toBe(true);
    expect(LHD.filterUser({gender:'female', dob:dob}, {gender:'any', min:18, max:90})).toBe(true);
});

test('Filters correctly on age', ()=>{
    const dobRight = "1983-07-14 07:29:45";
    const dobOld = "1883-07-14 07:29:45";
    const dobYoung = "2014-07-14 07:29:45";
    //too young
    expect(LHD.filterUser({gender:'male', dob:dobYoung}, {gender:'any', min:18, max:90})).toBe(false);
    //too old
    expect(LHD.filterUser({gender:'male', dob:dobOld}, {gender:'any', min:18, max:90})).toBe(false);
    //correct age
    expect(LHD.filterUser({gender:'male', dob:dobRight}, {gender:'any', min:18, max:90})).toBe(true);
});

test('Parses Age Correctly', ()=>{
    const dateString = "1983-07-14 07:29:45";
    expect(LHD.parseAge(dateString)).toBe(34);
});

test('Generates candidate HTML correctly', ()=>{
    const user1 = {picture:{large:"url1"}, name:{first:"John", last:"Doe"}, dob:"1983-07-14 07:29:45", phone:"1", cell:"2", email:"3"};
    const html1 = "<li>\n" +
        "                <img class=\"photo\" src=\"url1\">\n" +
        "                <span class=\"name lead\">John Doe</span>\n" +
        "                <span class=\"age lead\">34</span>\n" +
        "                <button onclick=\"alert('Phone: 1\\nCell: 2\\nEmail: 3')\">Contact</button>\n" +
        "              </li>";
    expect(LHD.generateCandidate(user1)).toBe(html1);
});

test('Function parseForm should return form values', ()=>{
    document.body.innerHTML = "<div class=\"form\">\n" +
        "          <div class=\"form-group\">\n" +
        "            <label class=\"control-label\">Age</label>\n" +
        "            <input type=\"text\" id=\"ageMin\" value=\"18\"> to\n" +
        "            <input type=\"text\" id=\"ageMax\" value=\"90\">\n" +
        "          </div>\n" +
        "\n" +
        "          <div class=\"form-group\">\n" +
        "            <label class=\"control-label\">Gender</label>\n" +
        "            <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" id=\"radioButtonAny\" value=\"any\" checked />Any</label>\n" +
        "            <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" value=\"male\"/>Male</label>\n" +
        "            <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" value=\"female\"/>Female</label>\n" +
        "          </div>\n";

    const newMinAge = 20;
    const newMaxAge = 30;
    //Test default values
    expect(LHD.parseForm()).toEqual({min:"18", max:"90", gender:"any"});

    //Change values and make sure it gets the new ones
    document.getElementById("ageMin").value = newMinAge;
    document.getElementById("ageMax").value = newMaxAge;
    expect(LHD.parseForm()).toEqual({min:`${newMinAge}`, max:`${newMaxAge}`, gender:"any"});
});

test('Function resetForm should set form back to default values', ()=>{
    const expectedOriginalForm = {min:"30", max:"80", gender:"any"};
    const expectedResetForm = {min:"18", max:"90", gender:"any"};
    //Note, this is not exactly the same HTML as the actual page. Default values have been changed for ease of use
    document.body.innerHTML = "<div class=\"form\">\n" +
        "          <div class=\"form-group\">\n" +
        "            <label class=\"control-label\">Age</label>\n" +
        "            <input type=\"text\" id=\"ageMin\" value=\"30\"> to\n" +
        "            <input type=\"text\" id=\"ageMax\" value=\"80\">\n" +
        "          </div>\n" +
        "\n" +
        "          <div class=\"form-group\">\n" +
        "            <label class=\"control-label\">Gender</label>\n" +
        "            <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" id=\"radioButtonAny\" value=\"any\" checked  />Any</label>\n" +
        "            <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" value=\"male\"/>Male</label>\n" +
        "            <label class=\"radio-inline\"><input type=\"radio\" name=\"gender\" value=\"female\"/>Female</label>\n" +
        "          </div>\n";
    expect(LHD.parseForm()).toEqual(expectedOriginalForm);
    LHD.resetForm();
    expect(LHD.parseForm()).toEqual(expectedResetForm);
});