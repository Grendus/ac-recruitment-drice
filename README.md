Hey,

You'll see two directories here. I did the original challenge in raw Javascript, which has full unit tests for everything I was able to simulate.

I then went back and redid the application using Angular 1.6.9, but ran out of time before I could get to unit testing on that one as it was a last minute inspiration.

-Dale