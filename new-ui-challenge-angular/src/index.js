// YOUR CODE HERE
let angular = require('angular');
angular.module("LHD",[])
    .controller("LHDController", function($scope, $http){
        $scope.filter = {minAge:18, maxAge:90, gender:"any"};
        $scope.users = [];
        $scope.filteredUsers = [];
        $http.get("https://randomuser.me/api/?results=10")
            .then((response)=>{
                $scope.users = response.data.results;
                $scope.users.forEach((user)=>{
                    user.age = Math.abs((new Date((Date.now() - new Date(Date.parse(user.dob)).getTime())).getUTCFullYear() - 1970));
                });
                $scope.filterUsers();
            });

        $scope.filterUsers = ()=>{
            $scope.filteredUsers = $scope.users.filter((user)=>{
                if ($scope.filter.gender === "any" || $scope.filter.gender === user.gender) {
                    return user.age >= $scope.filter.minAge && user.age <= $scope.filter.maxAge;
                }
                return false;
            });
        };

        $scope.contact = (user)=>alert("Phone: "+user.phone+"\nCell: "+user.cell+"\nEmail: "+user.email);
    });